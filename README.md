# Treasure Hunter

This application is a proof of concept for the [Entity Component System (ECS)](https://en.wikipedia.org/wiki/Entity_component_system) architectural pattern widely used in game development.

## Overview

Treasure Hunter is a simulation of a game where adventurers wander around picking up treasures scattered on a map.
The application showcases the ECS pattern's composition over inheritance principle and features:
- a core `Game` that loads a new game and executes the game loop;
- a text-based game state `SaveSystem`;
- a `MovementSystem` that operates on two collections of entities: 
    - `GameMap` for a grid map composed of tile entities (treasures and mountains); and
    - `Sprites` that contains all the sprite entities in the game (only adventurers);
- `FileGameState` which is the file-based implementation for both saving and loading operations;
- a basic `Entity` for which `Component` objects could be attributed; and
- several concrete `Component` derivatives that match the required attributes for the different `Entity` objects that could live in the game.

## Running the application

Treasure Hunter is a Java application that expects two optional arguments:
- an input file path: the file that contains the initial state of the simulation (default value: `samples/sample_01.in`); and
- an output file path to which the final state will be saved (default value: `samples/sample_01.out`).

## Todo list

Some crucial mechanisms are missing in this application and are deferred to a subsequent version, namely:
- a proper error handling: currently, algorithms detect and ignore errors;
- a log system;
- component-based collection of entities instead of the sprite vs tile division used in the code right now; and
- visuals.
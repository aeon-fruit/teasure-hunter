package edu.aeon.game.system;

import edu.aeon.game.component.*;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import edu.aeon.state.GameStateSaver;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SaveSystemTest {

    Random random = new Random();

    @Test
    void update_null() {
        GameMap defaultMap = new GameMap(1, 1);
        Sprites defaultSprites = new Sprites();
        final List<String> output = new ArrayList<>();
        GameStateSaver defaultSaver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        for (int it = 0; it < 50000; ++it) {
            GameMap map = defaultMap;
            Sprites sprites = defaultSprites;
            GameStateSaver saver = defaultSaver;
            switch (random.nextInt(7)) {
                case 0 -> map = null;
                case 1 -> sprites = null;
                case 2 -> saver = null;
                case 3 -> {
                    map = null;
                    sprites = null;
                }
                case 4 -> {
                    map = null;
                    saver = null;
                }
                case 5 -> {
                    sprites = null;
                    saver = null;
                }
                case 6 -> {
                    map = null;
                    sprites = null;
                    saver = null;
                }
            }
            SaveSystem system = new SaveSystem(map, sprites, saver);

            system.update();

            assertTrue(output.isEmpty());
        }
    }

    @Test
    void update_emptyMap() {
        GameMap[] maps = new GameMap[]{
                new GameMap(1, 0),
                new GameMap(0, 1),
                new GameMap(0, 0)
        };
        Sprites sprites = new Sprites();
        int count = random.nextInt(10) + 1;
        for (int it = 0; it < count; ++it) {
            sprites.add(EntityFactory.create().withComponent(new Savable('A')));
        }
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        for (GameMap map : maps) {
            SaveSystem system = new SaveSystem(map, sprites, saver);
            Dimension size = map.getSize();
            String expected = "C - " + size.width + " - " + size.height;

            system.update();

            assertEquals(1, output.size());
            assertEquals(expected, output.get(0));
        }
    }

    @Test
    void update_emptySprites() {
        Sprites sprites = new Sprites();
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            SaveSystem system = new SaveSystem(map, sprites, saver);
            List<String> expected = new ArrayList<>();
            expected.add("C - " + width + " - " + height);
            int count = random.nextInt((width * height / 2));
            for (int jt = 0; jt < count; ++jt) {
                boolean treasure = random.nextBoolean();
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.tileAt(x, y) != null);
                char symbol = treasure ? 'T' : 'M';
                int treasureCount = random.nextInt(10);
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Savable(symbol))
                        .withComponent(treasure ? new TreasureHolder(treasureCount, false) : null)
                        .withComponent(treasure ? new Walkable() : null)
                );
                expected.add(symbol + " - " + x + " - " + y + (treasure ? " - " + treasureCount : ""));
            }

            system.update();

            assertEquals(count + 1, output.size());
            Collections.sort(expected);
            Collections.sort(output);
            assertIterableEquals(expected, output);
        }
    }

    @Test
    void update_nonSavable() {
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        Moving.Direction[] directions = new Moving.Direction[]{
                Moving.Direction.East, Moving.Direction.South, Moving.Direction.West, Moving.Direction.North
        };
        Moving.Move[] moveSet = new Moving.Move[]{Moving.Move.Forward, Moving.Move.Right, Moving.Move.Left};
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            SaveSystem system = new SaveSystem(map, sprites, saver);
            String expected = "C - " + width + " - " + height;
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            for (int jt = 0; jt < spriteCount; ++jt) {
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.spriteAt(x, y) != null);
                String name = "Adventurer " + jt;
                Moving.Direction direction = directions[random.nextInt(4)];
                int movesCount = random.nextInt(11);
                Moving.Move[] moves = new Moving.Move[movesCount];
                for (int kt = 0; kt < movesCount; ++kt) {
                    moves[kt] = moveSet[random.nextInt(3)];
                }
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Moving(direction, moves))
                        .withComponent(new Named(name))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, y, sprite.getId());
            }
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                boolean treasure = random.nextBoolean();
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.tileAt(x, y) != null);
                int treasureCount = random.nextInt(10);
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, y))
                        .withComponent(treasure ? new TreasureHolder(treasureCount, false) : null)
                );
            }

            system.update();

            assertEquals(1, output.size());
            assertEquals(expected, output.get(0));
        }
    }

    @Test
    void update_nonEmpty() {
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        Moving.Direction[] directions = new Moving.Direction[]{
                Moving.Direction.East, Moving.Direction.South, Moving.Direction.West, Moving.Direction.North
        };
        Moving.Move[] moveSet = new Moving.Move[]{Moving.Move.Forward, Moving.Move.Right, Moving.Move.Left};
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            SaveSystem system = new SaveSystem(map, sprites, saver);
            List<String> expected = new ArrayList<>();
            expected.add("C - " + width + " - " + height);
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            for (int jt = 0; jt < spriteCount; ++jt) {
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.spriteAt(x, y) != null);
                String name = "Adventurer " + jt;
                Moving.Direction direction = directions[random.nextInt(4)];
                int movesCount = random.nextInt(11);
                Moving.Move[] moves = new Moving.Move[movesCount];
                for (int kt = 0; kt < movesCount; ++kt) {
                    moves[kt] = moveSet[random.nextInt(3)];
                }
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Moving(direction, moves))
                        .withComponent(new Named(name))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, y, sprite.getId());
                expected.add("A - " + name + " - " + x + " - " + y + " - " + direction.toSymbol() + " - 0");
            }
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                boolean treasure = random.nextBoolean();
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.tileAt(x, y) != null);
                char symbol = treasure ? 'T' : 'M';
                int treasureCount = random.nextInt(10);
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Savable(symbol))
                        .withComponent(treasure ? new TreasureHolder(treasureCount, false) : null)
                );
                expected.add(symbol + " - " + x + " - " + y + (treasure ? " - " + treasureCount : ""));
            }

            system.update();

            assertEquals(spriteCount + tileCount + 1, output.size());
            Collections.sort(expected);
            Collections.sort(output);
            assertIterableEquals(expected, output);
        }
    }

}
package edu.aeon.game.system;

import edu.aeon.game.component.*;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class MovementSystemTest {

    Random random = new Random();
    Moving.Direction[] directions = new Moving.Direction[]{
            Moving.Direction.East, Moving.Direction.South, Moving.Direction.West, Moving.Direction.North
    };
    Moving.Move[] moveSet = new Moving.Move[]{Moving.Move.Right, Moving.Move.Left, Moving.Move.Forward};

    @Test
    void update_null() {
        GameMap defaultMap = new GameMap(1, 1);
        Sprites defaultSprites = new Sprites();
        for (int it = 0; it < 50000; ++it) {
            GameMap map = defaultMap;
            Sprites sprites = defaultSprites;
            switch (random.nextInt(3)) {
                case 0 -> map = null;
                case 1 -> sprites = null;
                case 2 -> {
                    map = null;
                    sprites = null;
                }
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertFalse(system.isAlive());

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_emptyMap() {
        GameMap[] maps = new GameMap[]{
                new GameMap(1, 0),
                new GameMap(0, 1),
                new GameMap(0, 0)
        };
        Sprites sprites = new Sprites();
        int count = random.nextInt(10) + 1;
        for (int it = 0; it < count; ++it) {
            createSprite(10, 10, null, sprites, "Adventurer " + it, createMoves());
        }
        for (GameMap map : maps) {
            MovementSystem system = new MovementSystem(map, sprites);

            assertFalse(system.isAlive());

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_emptySprites() {
        Sprites sprites = new Sprites();
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            int count = random.nextInt((width * height / 2));
            for (int jt = 0; jt < count; ++jt) {
                createRandomTile(width, height, map);
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertFalse(system.isAlive());

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_missingComponents() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                createRandomTile(width, height, map);
            }
            Sprites sprites = new Sprites();
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            for (int jt = 0; jt < spriteCount; ++jt) {
                int x;
                int y;
                do {
                    x = random.nextInt(width);
                    y = random.nextInt(height);
                } while (map.spriteAt(x, y) != null || map.tileAt(x, y) != null);
                String name = "Adventurer " + jt;
                Moving.Direction direction = directions[random.nextInt(4)];
                int movesCount = random.nextInt(11);
                Moving.Move[] moves = new Moving.Move[movesCount];
                for (int kt = 0; kt < movesCount; ++kt) {
                    moves[kt] = moveSet[random.nextInt(3)];
                }
                int flag = random.nextInt(2) + 1;
                Entity sprite = EntityFactory.create()
                        .withComponent((flag & 0x1) == 0x1 ? null : new Locatable(x, y))
                        .withComponent((flag & 0x2) == 0x2 ? null : new Moving(direction, moves))
                        .withComponent(new Named(name))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, y, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertFalse(system.isAlive());

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_noMoves() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                createRandomTile(width, height, map);
            }
            Sprites sprites = new Sprites();
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            for (int jt = 0; jt < spriteCount; ++jt) {
                createSprite(width, height, map, sprites, "Adventurer " + jt, null);
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertFalse(system.isAlive());

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_exhaustAllMoves() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                createRandomTile(width, height, map);
            }
            Sprites sprites = new Sprites();
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            int maxMovesCount = 0;
            for (int jt = 0; jt < spriteCount; ++jt) {
                Moving.Move[] moves = createMoves();
                createSprite(width, height, map, sprites, "Adventurer " + jt, moves);
                maxMovesCount = Math.max(maxMovesCount, moves.length);
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < maxMovesCount; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_noCollision() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            int tileCount = random.nextInt((width * height / 2 + 1));
            for (int jt = 0; jt < tileCount; ++jt) {
                createRandomTile(width, height, map);
            }
            Sprites sprites = new Sprites();
            int spriteCount = random.nextInt((width * height / 10 + 1)) + 1;
            int maxMovesCount = 0;
            for (int jt = 0; jt < spriteCount; ++jt) {
                Moving.Move[] moves = createTurningMoves();
                createSprite(width, height, map, sprites, "Adventurer " + jt, moves);
                maxMovesCount = Math.max(maxMovesCount, moves.length);
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < maxMovesCount; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
        }
    }

    @Test
    void update_moveOutsideTheGrid() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(9) + 2;
            int height = random.nextInt(9) + 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            for (int x = 0; x < width; x += 2) {
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.South, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            for (int y = 1; y < height; y += 2) {
                Moving.Move[] moves = new Moving.Move[width * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(0, y))
                        .withComponent(new Moving(Moving.Direction.East, moves))
                        .withComponent(new Named("Adventurer " + y))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(0, y, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < Math.max(width, height) * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long spritesAtEdgesCount = map.streamLocations()
                    .filter(location -> map.spriteAt(location.getX(), location.getY()) != null)
                    .filter(location -> location.getX() == 0 || location.getX() == width - 1 || location.getY() == 0 || location.getY() == height - 1)
                    .count();
            assertEquals(map.streamSprites().count(), spritesAtEdgesCount);
        }
    }

    @Test
    void update_collisionWithSprites() {
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            for (int x = 0; x < width; ++x) {
                for (int y = 0; y < height; y += height - 1) {
                    Moving.Move[] moves = new Moving.Move[height * 2];
                    Arrays.fill(moves, Moving.Move.Forward);
                    Entity sprite = EntityFactory.create()
                            .withComponent(new Locatable(x, y))
                            .withComponent(new Moving(y == 0 ? Moving.Direction.South : Moving.Direction.North, moves))
                            .withComponent(new Named("Adventurer " + x + y))
                            .withComponent(new Savable('A'))
                            .withComponent(new TreasureHolder(0, true));
                    sprites.add(sprite);
                    map.addSprite(x, y, sprite.getId());
                }
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < height * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long spritesOnEquator = map.streamLocations()
                    .filter(location -> map.spriteAt(location.getX(), location.getY()) != null)
                    .filter(location -> location.getY() == height / 2 || location.getY() == height / 2 - 1)
                    .count();
            assertEquals(map.streamSprites().count(), spritesOnEquator);
        }
    }

    @Test
    void update_collisionWithTiles() {
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            for (int x = 0; x < width; ++x) {
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, height / 2))
                        .withComponent(new Savable('M'))
                );
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.South, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < height * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long spritesOnEquator = map.streamLocations()
                    .filter(location -> map.spriteAt(location.getX(), location.getY()) != null)
                    .filter(location -> location.getY() == height / 2 - 1)
                    .count();
            assertEquals(map.streamSprites().count(), spritesOnEquator);
        }
    }

    @Test
    void update_enterWalkable() {
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            Map<Long, Integer> treasureRegistry = new HashMap<>();
            for (int x = 0; x < width; ++x) {
                int treasureCount = random.nextInt(3) + 1;
                Entity treasureChest = EntityFactory.create()
                        .withComponent(new Locatable(x, height - 1))
                        .withComponent(new Savable('T'))
                        .withComponent(new TreasureHolder(treasureCount, false))
                        .withComponent(new Walkable());
                map.addTile(treasureChest);
                treasureRegistry.put(treasureChest.getId(), treasureCount);
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.South, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < height * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long richSprites = map.streamLocations()
                    .map(location -> map.spriteAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .map(sprites::getById)
                    .map(sprite -> sprite.getComponent(TreasureHolder.class))
                    .filter(Objects::nonNull)
                    .filter(th -> th.getCount() == 1)
                    .count();
            assertEquals(width, richSprites);
            long plunderedChests = map.streamLocations()
                    .map(location -> map.tileAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .filter(sprite -> {
                        TreasureHolder th = sprite.getComponent(TreasureHolder.class);
                        return th != null && th.getCount() + 1 == treasureRegistry.get(sprite.getId());
                    })
                    .count();
            long exhaustedChests = treasureRegistry.values().stream().filter(count -> count == 1).count();
            assertEquals(width, plunderedChests + exhaustedChests);
        }
    }

    @Test
    void update_leaveWalkable() {
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            Map<Long, Integer> treasureRegistry = new HashMap<>();
            for (int x = 0; x < width; ++x) {
                int treasureCount = random.nextInt(3) + 1;
                Entity treasureChest = EntityFactory.create()
                        .withComponent(new Locatable(x, height / 2))
                        .withComponent(new Savable('T'))
                        .withComponent(new TreasureHolder(treasureCount, false))
                        .withComponent(new Walkable());
                map.addTile(treasureChest);
                treasureRegistry.put(treasureChest.getId(), treasureCount);
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.South, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < height * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long richSprites = map.streamLocations()
                    .map(location -> map.spriteAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .map(sprites::getById)
                    .map(sprite -> sprite.getComponent(TreasureHolder.class))
                    .filter(Objects::nonNull)
                    .filter(th -> th.getCount() == 1)
                    .count();
            assertEquals(width, richSprites);
            long plunderedChests = map.streamLocations()
                    .map(location -> map.tileAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .filter(sprite -> {
                        TreasureHolder th = sprite.getComponent(TreasureHolder.class);
                        return th != null && th.getCount() + 1 == treasureRegistry.get(sprite.getId());
                    })
                    .count();
            long exhaustedChests = treasureRegistry.values().stream().filter(count -> count == 1).count();
            assertEquals(width, plunderedChests + exhaustedChests);
        }
    }

    @Test
    void update_reenterWalkable() {
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            for (int x = 0; x < width; ++x) {
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, height / 2))
                        .withComponent(new Savable('T'))
                        .withComponent(new TreasureHolder(2, false))
                        .withComponent(new Walkable())
                );
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, 0, height - 1, Moving.Move.Forward);
                moves[height - 1] = Moving.Move.Right;
                moves[height] = Moving.Move.Right;
                Arrays.fill(moves, height + 1, height * 2, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.South, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            MovementSystem system = new MovementSystem(map, sprites);

            assertTrue(system.isAlive());

            for (int jt = 0; jt < height * 2; ++jt) {
                system.update();

                assertTrue(system.isAlive());
            }

            system.update();

            assertFalse(system.isAlive());
            long richSprites = map.streamLocations()
                    .map(location -> map.spriteAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .map(sprites::getById)
                    .map(sprite -> sprite.getComponent(TreasureHolder.class))
                    .filter(Objects::nonNull)
                    .filter(th -> th.getCount() == 2)
                    .count();
            assertEquals(width, richSprites);
            long plunderedChests = map.streamLocations()
                    .map(location -> map.tileAt(location.getX(), location.getY()))
                    .filter(Objects::nonNull)
                    .map(sprite -> sprite.getComponent(TreasureHolder.class))
                    .filter(Objects::nonNull)
                    .filter(th -> th.getCount() == 0)
                    .count();
            assertEquals(width, plunderedChests);
        }
    }

    private void createRandomTile(int width, int height, GameMap map) {
        if (random.nextBoolean()) {
            createTreasureTile(width, height, map);
        } else {
            createMountainTile(width, height, map);
        }
    }

    private void createTreasureTile(int width, int height, GameMap map) {
        createTile(width, height, map, true);
    }

    private void createMountainTile(int width, int height, GameMap map) {
        createTile(width, height, map, false);
    }

    private void createTile(int width, int height, GameMap map, boolean treasure) {
        int x;
        int y;
        do {
            x = random.nextInt(width);
            y = random.nextInt(height);
        } while (map.tileAt(x, y) != null);
        char symbol = treasure ? 'T' : 'M';
        int treasureCount = random.nextInt(10);
        map.addTile(EntityFactory.create()
                .withComponent(new Locatable(x, y))
                .withComponent(new Savable(symbol))
                .withComponent(treasure ? new TreasureHolder(treasureCount, false) : null)
                .withComponent(treasure ? new Walkable() : null)
        );
    }

    private Moving.Move[] createMoves() {
        int movesCount = random.nextInt(10) + 1;
        Moving.Move[] moves = new Moving.Move[movesCount];
        for (int kt = 0; kt < movesCount; ++kt) {
            moves[kt] = moveSet[random.nextInt(3)];
        }
        return moves;
    }

    private Moving.Move[] createTurningMoves() {
        int movesCount = random.nextInt(10) + 1;
        Moving.Move[] moves = new Moving.Move[movesCount];
        for (int kt = 0; kt < movesCount; ++kt) {
            moves[kt] = moveSet[random.nextInt(2)];
        }
        return moves;
    }

    private void createSprite(int width, int height, GameMap map, Sprites sprites, String name, Moving.Move[] moves) {
        int x;
        int y;
        do {
            x = random.nextInt(width);
            y = random.nextInt(height);
        } while ((map != null) && ((map.spriteAt(x, y) != null) || (map.tileAt(x, y) != null)));
        Moving.Direction direction = directions[random.nextInt(4)];
        Entity sprite = EntityFactory.create()
                .withComponent(new Locatable(x, y))
                .withComponent(new Moving(direction, moves))
                .withComponent(new Named(name))
                .withComponent(new Savable('A'))
                .withComponent(new TreasureHolder(0, true));
        sprites.add(sprite);
        if (map != null) {
            map.addSprite(x, y, sprite.getId());
        }
    }

}
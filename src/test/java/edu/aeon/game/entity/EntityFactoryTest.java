package edu.aeon.game.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EntityFactoryTest {

    @Test
    void create() {
        for (long expected = 0; expected < 50000; ++expected) {
            Entity entity = EntityFactory.create();

            assertEquals(expected, entity.getId());
        }
    }

}
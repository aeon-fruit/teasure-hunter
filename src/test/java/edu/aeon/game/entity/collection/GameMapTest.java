package edu.aeon.game.entity.collection;

import edu.aeon.game.component.Locatable;
import edu.aeon.game.component.Walkable;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import edu.aeon.game.util.Location;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class GameMapTest {

    Random random = new Random();

    @Test
    void constructor() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);

        assertEquals(0, map.streamTiles().count());
        assertEquals(width * height, map.streamLocations().count());
        assertEquals(0, map.streamSprites().count());
    }

    @Test
    void getSize_empty() {
        for (int i = -1; i < 2; ++i) {
            for (int j = -1; j < 2 && Math.max(i, 0) * j < 1; ++j) {
                GameMap map = new GameMap(i, j);

                Dimension size = map.getSize();

                assertEquals(0, size.width);
                assertEquals(0, size.height);
                assertEquals(0, map.streamLocations().count());
            }
        }
    }

    @Test
    void getSize_nonEmpty() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 1;
            int height = random.nextInt(100) + 1;
            GameMap map = new GameMap(width, height);

            Dimension size = map.getSize();

            assertEquals(width, size.width);
            assertEquals(height, size.height);
        }
    }

    @Test
    void addTile_empty() {
        GameMap map = new GameMap(0, 0);
        boolean added;
        for (int it = 0; it < 50000; ++it) {
            int x = random.nextInt(100);
            int y = random.nextInt(100);

            added = map.addTile(EntityFactory.create().withComponent(new Locatable(x, y)));

            assertFalse(added);
            assertEquals(0, map.streamTiles().count());
        }
    }

    @Test
    void addTile_null() {
        GameMap map = new GameMap(random.nextInt(100) + 1, random.nextInt(100) + 1);
        boolean added;
        for (int it = 0; it < 50000; ++it) {

            added = map.addTile(null);

            assertFalse(added);
            assertEquals(0, map.streamTiles().count());
        }
    }

    @Test
    void addTile_nonLocatable() {
        GameMap map = new GameMap(random.nextInt(100) + 1, random.nextInt(100) + 1);
        boolean added;
        for (int it = 0; it < 50000; ++it) {

            added = map.addTile(EntityFactory.create().withComponent(new Walkable()));

            assertFalse(added);
            assertEquals(0, map.streamTiles().count());
        }
    }

    @Test
    void addTile_outOfBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            if (random.nextBoolean()) {
                if (random.nextBoolean()) {
                    x = random.nextInt(width);
                    y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                } else {
                    x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                    y = random.nextInt(height);
                }
            } else {
                x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
            }

            added = map.addTile(EntityFactory.create().withComponent(new Locatable(x, y)));

            assertFalse(added);
            assertEquals(0, map.streamTiles().count());
        }
    }

    @Test
    void addTile_inBounds() {
        int width = random.nextInt(1000) + 1;
        int height = random.nextInt(1000) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 500; ++it) {
            int x;
            int y;
            do {
                x = random.nextInt(width);
                y = random.nextInt(height);
            } while (map.tileAt(x, y) != null);
            Entity tile = EntityFactory.create().withComponent(new Locatable(x, y));

            added = map.addTile(tile);

            assertTrue(added);
            assertEquals(it + 1, map.streamTiles().count());
            assertEquals(tile, map.tileAt(x, y));
        }
    }

    @Test
    void removeTile_null() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean removed;
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        Entity tile = EntityFactory.create().withComponent(new Locatable(x, y));
        map.addTile(tile);
        for (int it = 0; it < 50000; ++it) {

            removed = map.removeTile(null);

            assertFalse(removed);
            assertEquals(1, map.streamTiles().count());
            assertEquals(tile, map.tileAt(x, y));
        }
    }

    @Test
    void removeTile_nonLocatable() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean removed;
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            do {
                x = random.nextInt(width);
                y = random.nextInt(height);
            } while (map.tileAt(x, y) != null);
            Entity tile = EntityFactory.create().withComponent(new Walkable());
            map.addTile(tile);

            removed = map.removeTile(tile);

            assertFalse(removed);
            assertEquals(0, map.streamTiles().count());
            assertNull(map.tileAt(x, y));
        }
    }

    @Test
    void removeTile_outOfBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean removed;
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            if (random.nextBoolean()) {
                if (random.nextBoolean()) {
                    x = random.nextInt(width);
                    y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                } else {
                    x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                    y = random.nextInt(height);
                }
            } else {
                x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
            }
            Entity tile = EntityFactory.create().withComponent(new Walkable());
            map.addTile(tile);

            removed = map.removeTile(tile);

            assertFalse(removed);
            assertEquals(0, map.streamTiles().count());
            assertNull(map.tileAt(x, y));
        }
    }

    @Test
    void removeTile_inBounds() {
        int width = random.nextInt(1000) + 1;
        int height = random.nextInt(1000) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 500; ++it) {
            int x;
            int y;
            do {
                x = random.nextInt(width);
                y = random.nextInt(height);
            } while (map.tileAt(x, y) != null);
            Entity tile = EntityFactory.create().withComponent(new Locatable(x, y));
            map.addTile(tile);

            added = map.removeTile(tile);

            assertTrue(added);
            assertEquals(0, map.streamTiles().count());
            assertNull(map.tileAt(x, y));
        }
    }

    @Test
    void tileAt_outOfBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                map.addTile(EntityFactory.create().withComponent(new Locatable(x, y)));
            }
        }
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            if (random.nextBoolean()) {
                if (random.nextBoolean()) {
                    x = random.nextInt(width);
                    y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                } else {
                    x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                    y = random.nextInt(height);
                }
            } else {
                x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
            }

            Entity tile = map.tileAt(x, y);

            assertNull(tile);
        }
    }

    @Test
    void tileAt_inBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                map.addTile(EntityFactory.create().withComponent(new Locatable(x, y)));
            }
        }
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {

                Entity tile = map.tileAt(x, y);

                assertNotNull(tile);
            }
        }
    }

    @Test
    void addSprite_empty() {
        GameMap map = new GameMap(0, 0);
        boolean added;
        for (int it = 0; it < 50000; ++it) {
            int x = random.nextInt(100);
            int y = random.nextInt(100);

            added = map.addSprite(x, y, EntityFactory.create().getId());

            assertFalse(added);
            assertEquals(0, map.streamSprites().count());
        }
    }

    @Test
    void addSprite_null() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 50000; ++it) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);

            added = map.addSprite(x, y, null);

            assertFalse(added);
            assertEquals(0, map.streamSprites().count());
        }
    }

    @Test
    void addSprite_outOfBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            if (random.nextBoolean()) {
                if (random.nextBoolean()) {
                    x = random.nextInt(width);
                    y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                } else {
                    x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                    y = random.nextInt(height);
                }
            } else {
                x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
            }

            added = map.addSprite(x, y, EntityFactory.create().getId());

            assertFalse(added);
            assertEquals(0, map.streamSprites().count());
        }
    }

    @Test
    void addSprite_inBounds() {
        int width = random.nextInt(1000) + 1;
        int height = random.nextInt(1000) + 1;
        GameMap map = new GameMap(width, height);
        boolean added;
        for (int it = 0; it < 500; ++it) {
            int x;
            int y;
            do {
                x = random.nextInt(width);
                y = random.nextInt(height);
            } while (map.spriteAt(x, y) != null);
            Entity sprite = EntityFactory.create();

            added = map.addSprite(x, y, sprite.getId());

            assertTrue(added);
            assertEquals(it + 1, map.streamSprites().count());
            assertEquals(sprite.getId(), map.spriteAt(x, y));
        }
    }

    @Test
    void spriteAt_outOfBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                map.addSprite(x, y, EntityFactory.create().getId());
            }
        }
        for (int it = 0; it < 50000; ++it) {
            int x;
            int y;
            if (random.nextBoolean()) {
                if (random.nextBoolean()) {
                    x = random.nextInt(width);
                    y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                } else {
                    x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                    y = random.nextInt(height);
                }
            } else {
                x = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
                y = (random.nextBoolean() ? random.nextInt(100) + 100 : -random.nextInt(100) - 1);
            }

            Long spriteId = map.spriteAt(x, y);

            assertNull(spriteId);
        }
    }

    @Test
    void spriteAt_inBounds() {
        int width = random.nextInt(100) + 1;
        int height = random.nextInt(100) + 1;
        GameMap map = new GameMap(width, height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                map.addSprite(x, y, EntityFactory.create().getId());
            }
        }
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {

                Long spriteId = map.spriteAt(x, y);

                assertNotNull(spriteId);
            }
        }
    }

    @Test
    void moveSprite_null() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int oldX = random.nextInt(width / 2);
            int oldY = random.nextInt(height / 2);
            int x = random.nextInt(width / 2) + (width / 2);
            int y = random.nextInt(height / 2) + (height / 2);

            map.moveSprite(oldX, oldY, x, y);

            assertEquals(0, map.streamSprites().count());
            assertNull(map.spriteAt(oldX, oldY));
            assertNull(map.spriteAt(x, y));
        }
    }

    @Test
    void moveSprite_sameX() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int oldX = random.nextInt(width / 2);
            int oldY = random.nextInt(height / 2);
            int y = random.nextInt(height / 2) + (height / 2);
            Entity sprite = EntityFactory.create();
            map.addSprite(oldX, oldY, sprite.getId());

            map.moveSprite(oldX, oldY, oldX, y);

            assertEquals(1, map.streamSprites().count());
            assertNull(map.spriteAt(oldX, oldY));
            assertEquals(sprite.getId(), map.spriteAt(oldX, y));
        }
    }

    @Test
    void moveSprite_sameY() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int oldX = random.nextInt(width / 2);
            int oldY = random.nextInt(height / 2);
            int x = random.nextInt(width / 2) + (width / 2);
            Entity sprite = EntityFactory.create();
            map.addSprite(oldX, oldY, sprite.getId());

            map.moveSprite(oldX, oldY, x, oldY);

            assertEquals(1, map.streamSprites().count());
            assertNull(map.spriteAt(oldX, oldY));
            assertEquals(sprite.getId(), map.spriteAt(x, oldY));
        }
    }

    @Test
    void moveSprite_sameXY() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int oldX = random.nextInt(width / 2);
            int oldY = random.nextInt(height / 2);
            Entity sprite = EntityFactory.create();
            map.addSprite(oldX, oldY, sprite.getId());

            map.moveSprite(oldX, oldY, oldX, oldY);

            assertEquals(1, map.streamSprites().count());
            assertEquals(sprite.getId(), map.spriteAt(oldX, oldY));
        }
    }

    @Test
    void moveSprite_differentXY() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int oldX = random.nextInt(width / 2);
            int oldY = random.nextInt(height / 2);
            int x = random.nextInt(width / 2) + (width / 2);
            int y = random.nextInt(height / 2) + (height / 2);
            Entity sprite = EntityFactory.create();
            map.addSprite(oldX, oldY, sprite.getId());

            map.moveSprite(oldX, oldY, x, y);

            assertEquals(1, map.streamSprites().count());
            assertNull(map.spriteAt(oldX, oldY));
            assertEquals(sprite.getId(), map.spriteAt(x, y));
        }
    }

    @Test
    void moveSprite_outOfBounds() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100) + 6;
            int height = random.nextInt(100) + 6;
            GameMap map = new GameMap(width, height);
            int flag = random.nextInt(8) + 1;
            int oldX = (flag & 0x1) == 0x1 ? random.nextInt(width / 2) : random.nextInt(width) + width + 1;
            int oldY = (flag & 0x2) == 0x2 ? random.nextInt(height / 2) : random.nextInt(height) + height + 1;
            int x = (flag & 0x4) == 0x4 ? random.nextInt(width / 2) + (width / 2) : random.nextInt(width) + width + 1;
            int y = (flag & 0x8) == 0x8 ? random.nextInt(height / 2) + (height / 2) : random.nextInt(height) + height + 1;
            Entity sprite = EntityFactory.create();
            int realX = (flag & 0x1) == 0x1 ? oldX : random.nextInt(width / 2);
            int realY = (flag & 0x2) == 0x2 ? oldY : random.nextInt(height / 2);
            map.addSprite(realX, realY, sprite.getId());

            map.moveSprite(oldX, oldY, x, y);

            assertEquals(1, map.streamSprites().count());
            assertEquals(sprite.getId(), map.spriteAt(realX, realY));
            assertNull(map.spriteAt(x, y));
        }
    }

    @Test
    void location() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(10);
            int height = random.nextInt(10);
            GameMap map = new GameMap(width, height);

            Location location = map.location(random.nextInt(), random.nextInt());

            assertTrue(location.getX() >= 0);
            assertTrue(width == 0 ? location.getX() <= width : location.getX() < width);
            assertTrue(location.getY() >= 0);
            assertTrue(height == 0 ? location.getY() <= height : location.getY() < height);
        }
    }

    @Test
    void streamLocations() {
        for (int it = 0; it < 50000; ++it) {
            int width = random.nextInt(100);
            int height = random.nextInt(100);
            int size = width * height;
            GameMap map = new GameMap(width, height);

            Stream<Location> locationStream = map.streamLocations();

            List<Location> locations = locationStream.collect(Collectors.toList());
            assertEquals(size, locations.size());
            List<Location> expected = new ArrayList<>();
            for (int y = 0; y < height; ++y) {
                for (int x = 0; x < width; ++x) {
                    expected.add(new Location(x, y));
                }
            }
            assertIterableEquals(expected, locations);
        }
    }

}
package edu.aeon.game.entity.collection;

import edu.aeon.game.component.Moving;
import edu.aeon.game.component.Savable;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class SpritesTest {

    @Test
    void add_null() {
        Sprites sprites = new Sprites();

        sprites.add(null);

        assertEquals(0, sprites.stream().count());
    }

    @Test
    void add_nonMoving() {
        Sprites sprites = new Sprites();
        Entity entity = EntityFactory.create().withComponent(new Savable('S'));

        sprites.add(entity);

        assertEquals(0, sprites.stream().count());
        assertNull(sprites.getById(entity.getId()));
    }

    @Test
    void add_Moving() {
        Sprites sprites = new Sprites();
        Entity entity = EntityFactory.create().withComponent(new Moving(Moving.Direction.South, null));

        sprites.add(entity);

        assertEquals(1, sprites.stream().count());
        assertEquals(entity, sprites.getById(entity.getId()));
    }

}
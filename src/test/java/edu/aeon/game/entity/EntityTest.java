package edu.aeon.game.entity;

import edu.aeon.game.component.Locatable;
import edu.aeon.game.component.Named;
import edu.aeon.game.component.Walkable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EntityTest {

    @Test
    void constructor() {
        Entity entity = new Entity(1L);

        assertEquals(0L, entity.stream().count());
    }

    @Test
    void withComponent_null() {
        Entity entity = new Entity(1L);

        entity.withComponent(null);
        entity.withComponent(null);

        assertEquals(0L, entity.stream().count());
    }

    @Test
    void withComponent_nonNull() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(null);
        entity.withComponent(new Named("test"));

        assertEquals(2L, entity.stream().count());
    }

    @Test
    void withComponent_uniqueness() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));
        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));
        entity.withComponent(new Walkable());

        assertEquals(2L, entity.stream().count());
    }

    @Test
    void getComponent_empty() {
        Entity entity = new Entity(1L);

        assertNull(entity.getComponent(Walkable.class));
    }

    @Test
    void getComponent_null() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));

        assertNull(entity.getComponent(null));
    }

    @Test
    void getComponent_nonExistent() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));

        assertNull(entity.getComponent(Locatable.class));
    }

    @Test
    void getComponent_Existent() {
        Entity entity = new Entity(1L);

        Walkable walkable = new Walkable();
        entity.withComponent(walkable);
        entity.withComponent(new Named("test"));

        assertEquals(walkable, entity.getComponent(Walkable.class));
    }

    @Test
    void getComponent_ExistentLast() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        Named first = new Named("first");
        Named last = new Named("last");
        entity.withComponent(first);
        entity.withComponent(last);

        assertEquals(last, entity.getComponent(Named.class));
    }

    @Test
    void hasComponent_empty() {
        Entity entity = new Entity(1L);

        assertFalse(entity.hasComponent(Walkable.class));
    }

    @Test
    void hasComponent_null() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));

        assertFalse(entity.hasComponent(Locatable.class));
    }

    @Test
    void hasComponent_nonExistent() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));

        assertFalse(entity.hasComponent(Locatable.class));
    }

    @Test
    void hasComponent_Existent() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("test"));

        assertTrue(entity.hasComponent(Walkable.class));
    }

    @Test
    void hasComponent_ExistentLast() {
        Entity entity = new Entity(1L);

        entity.withComponent(new Walkable());
        entity.withComponent(new Named("first"));
        entity.withComponent(new Named("last"));

        assertTrue(entity.hasComponent(Named.class));
    }

}
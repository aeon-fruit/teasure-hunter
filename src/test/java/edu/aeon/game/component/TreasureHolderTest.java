package edu.aeon.game.component;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class TreasureHolderTest {

    Random random = new Random();

    @Test
    void constructor_countPositive() {
        for (int it = 0; it < 50000; ++it) {
            int count = Math.abs(random.nextInt()) + 1;
            boolean incrementing = random.nextBoolean();
            TreasureHolder th = new TreasureHolder(count, incrementing);

            assertEquals(count, th.getCount());
            assertEquals(!incrementing, th.isGlimmering());
            assertEquals(incrementing, th.isClosed());
        }
    }

    @Test
    void constructor_countNegative() {
        for (int it = 0; it < 50000; ++it) {
            int count = -Math.abs(random.nextInt()) - 1;
            boolean incrementing = random.nextBoolean();
            TreasureHolder th = new TreasureHolder(count, incrementing);

            assertEquals(0, th.getCount());
            assertFalse(th.isGlimmering());
            assertTrue(th.isClosed());
        }
    }

    @Test
    void constructor_countZero() {
        for (int it = 0; it < 50000; ++it) {
            boolean incrementing = random.nextBoolean();
            TreasureHolder th = new TreasureHolder(0, incrementing);

            assertEquals(0, th.getCount());
            assertFalse(th.isGlimmering());
            assertTrue(th.isClosed());
        }
    }

    @Test
    void setClosed_countPositive() {
        for (int it = 0; it < 50000; ++it) {
            int count = Math.abs(random.nextInt()) + 1;
            boolean incrementing = random.nextBoolean();
            TreasureHolder th = new TreasureHolder(count, incrementing);

            for (int jt = 0; jt < 3000; ++jt) {
                boolean closed = random.nextBoolean();
                th.setClosed(closed);

                assertEquals(incrementing || closed, th.isClosed());
            }
        }
    }

    @Test
    void setClosed_countNegativeOrZero() {
        for (int it = 0; it < 50000; ++it) {
            int count = random.nextInt(100) > 95 ? 0 : -Math.abs(random.nextInt()) - 1;
            boolean incrementing = random.nextBoolean();
            TreasureHolder th = new TreasureHolder(count, incrementing);

            for (int jt = 0; jt < 3000; ++jt) {
                th.setClosed(random.nextBoolean());

                assertTrue(th.isClosed());
            }
        }
    }

    @Test
    void changeCount_incrementingNotClosed() {
        for (int it = 0; it < 50000; ++it) {
            int count = Math.abs(random.nextInt()) + 2;
            TreasureHolder th = new TreasureHolder(count, false);

            for (int jt = 0; jt < 3000; ++jt) {
                th.changeCount();
                --count;

                assertEquals(count, th.getCount());
                assertTrue(th.isGlimmering());
                assertFalse(th.isClosed());
            }
        }
    }

    @Test
    void changeCount_decrementingNotClosed() {
        for (int it = 0; it < 50000; ++it) {
            int count = random.nextInt(2990) + 1;
            TreasureHolder th = new TreasureHolder(count, false);

            for (int jt = 0; jt < 3000; ++jt) {
                th.changeCount();
                count = Math.max(0, count - 1);

                assertEquals(count, th.getCount());
                assertEquals(count > 0, th.isGlimmering());
                assertEquals(count <= 0, th.isClosed());
            }
        }
    }

    @Test
    void changeCount_closed() {
        for (int it = 0; it < 50000; ++it) {
            TreasureHolder th = new TreasureHolder(0, false);

            for (int jt = 0; jt < 3000; ++jt) {
                th.changeCount();

                assertEquals(0, th.getCount());
                assertFalse(th.isGlimmering());
                assertTrue(th.isClosed());
            }
        }
    }

}
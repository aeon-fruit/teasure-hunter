package edu.aeon.game.component;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WalkableTest {

    @Test
    void constructor() {
        Walkable walkable = new Walkable();

        assertFalse(walkable.getCollision());
        assertFalse(walkable.isToggled());
    }

    @Test
    void collide_falseToFalse() {
        Walkable walkable = new Walkable();

        walkable.collide(false);

        assertFalse(walkable.getCollision());
        assertFalse(walkable.isToggled());
    }

    @Test
    void collide_falseToTrue() {
        Walkable walkable = new Walkable();

        walkable.collide(true);

        assertTrue(walkable.getCollision());
        assertTrue(walkable.isToggled());
    }

    @Test
    void collide_trueToFalse() {
        Walkable walkable = new Walkable();
        walkable.collide(true);

        walkable.collide(false);

        assertFalse(walkable.getCollision());
        assertFalse(walkable.isToggled());
    }

    @Test
    void collide_trueToTrue() {
        Walkable walkable = new Walkable();
        walkable.collide(true);

        walkable.collide(true);

        assertTrue(walkable.getCollision());
        assertFalse(walkable.isToggled());
    }

}
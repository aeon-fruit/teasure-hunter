package edu.aeon.game.component;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MovingTest {

    @Test
    public void getNextMove_Null() {
        Moving moving = new Moving(Moving.Direction.East, null);

        assertNull(moving.getNextMove());
    }

    @Test
    public void getNextMove_Empty() {
        Moving moving = new Moving(Moving.Direction.East, new Moving.Move[]{});

        assertNull(moving.getNextMove());
    }

    @Test
    public void getNextMove_NonEmpty() {
        Random random = new Random();
        for (int it = 0; it < 50000; ++it) {
            int count = random.nextInt(100) + 1;
            Moving.Move[] moves = new Moving.Move[count];
            Moving.Move[] lookup = new Moving.Move[]{Moving.Move.Forward, Moving.Move.Right, Moving.Move.Left};
            for (int i = 0; i < count; ++i) {
                moves[i] = lookup[random.nextInt(3)];
            }
            Moving moving = new Moving(Moving.Direction.East, moves);

            List<Moving.Move> actual = new ArrayList<>();
            Moving.Move move = moving.getNextMove();
            while (move != null) {
                actual.add(move);
                move = moving.getNextMove();
            }

            assertIterableEquals(Arrays.asList(moves), actual);
        }
    }

}
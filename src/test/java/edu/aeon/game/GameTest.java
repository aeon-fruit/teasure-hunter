package edu.aeon.game;

import edu.aeon.game.component.*;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import edu.aeon.game.system.MovementSystem;
import edu.aeon.state.GameStateSaver;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.util.Collections.sort;
import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    Random random = new Random();

    @Test
    void loop() {
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            for (int x = 0; x < width; ++x) {
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, height - 1))
                        .withComponent(new Savable('M'))
                        .withComponent(new TreasureHolder(2, false))
                        .withComponent(new Walkable())
                );
                Moving.Move[] moves = new Moving.Move[height * 2];
                Arrays.fill(moves, Moving.Move.Forward);
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.North, moves))
                        .withComponent(new Named("Adventurer " + x))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
            }
            Game system = new Game(map, sprites, saver);

            system.loop();

            assertEquals(width * 2 + 1, output.size());
            MovementSystem movementSystem = new MovementSystem(map, sprites);
            assertFalse(movementSystem.isAlive());
        }
    }

    @Test
    void load() {
        final List<String> expectedOutput = new ArrayList<>();
        GameStateSaver expectedSaver = lines -> {
            expectedOutput.clear();
            expectedOutput.addAll(lines);
        };
        final List<String> output = new ArrayList<>();
        GameStateSaver saver = lines -> {
            output.clear();
            output.addAll(lines);
        };
        for (int it = 0; it < 50000; ++it) {
            int width = (random.nextInt(9) + 2) * 2;
            int height = (random.nextInt(9) + 2) * 2;
            GameMap map = new GameMap(width, height);
            Sprites sprites = new Sprites();
            List<String> input = new ArrayList<>();
            input.add("C - " + width + " - " + height);
            for (int x = 0; x < width; ++x) {
                input.add("# Line #" + x);
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, height - 1))
                        .withComponent(new Savable('M'))
                );
                input.add("M - " + x + " - " + (height - 1));
                map.addTile(EntityFactory.create()
                        .withComponent(new Locatable(x, height / 2))
                        .withComponent(new Savable('T'))
                        .withComponent(new TreasureHolder(2, false))
                        .withComponent(new Walkable())
                );
                input.add("T - " + x + " - " + (height / 2) + " - 2");
                Moving.Move[] moves = new Moving.Move[3 * height / 4];
                Arrays.fill(moves, Moving.Move.Forward);
                String name = "Adventurer " + x;
                Entity sprite = EntityFactory.create()
                        .withComponent(new Locatable(x, 0))
                        .withComponent(new Moving(Moving.Direction.North, moves))
                        .withComponent(new Named(name))
                        .withComponent(new Savable('A'))
                        .withComponent(new TreasureHolder(0, true));
                sprites.add(sprite);
                map.addSprite(x, 0, sprite.getId());
                input.add("A - " + name + " - " + x + " - " + 0 + " - " + Moving.Direction.North.toSymbol() + " - " +
                        Arrays.stream(moves).map(move -> move.toSymbol() + "").collect(Collectors.joining()));
            }
            Game original = new Game(map, sprites, expectedSaver);
            original.loop();

            Game game = Game.load(() -> input, saver);
            game.loop();

            assertEquals(expectedOutput.size(), output.size());
            sort(expectedOutput);
            sort(output);
            assertIterableEquals(expectedOutput, output);
        }
    }

}
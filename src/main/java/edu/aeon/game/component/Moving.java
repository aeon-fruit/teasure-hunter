package edu.aeon.game.component;

import java.util.Arrays;

/**
 * Moving is a component that holds a direction (East, South, North, West) and a set of moves (Forward, Right, Left) of an Entity object.
 */
public class Moving implements Component {

    private Direction direction;
    private final Moves moves;

    /**
     * Constructor that initializes direction and moves.
     *
     * @param direction direction.
     * @param moves     array of moves.
     */
    public Moving(Direction direction, Move[] moves) {
        this.direction = direction == null ? Direction.East : direction;
        this.moves = new Moves(moves);
    }

    /**
     * Get direction.
     *
     * @return direction.
     */
    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Get the next move without consuming it.
     *
     * @return next move if any, null otherwise.
     */
    public Move peekNextMove() {
        return moves.getCurrent();
    }

    /**
     * Get and consume the next move.
     *
     * @return next move if any, null otherwise.
     */
    public Move getNextMove() {
        return moves.getNext();
    }

    @Override
    public String toString() {
        return "Moving{" +
                "direction=" + direction +
                ", moves=" + moves +
                '}';
    }

    /**
     * Direction of a Moving Entity object.
     */
    public enum Direction {
        East, South, West, North;

        /**
         * Get a direction from its symbol.
         *
         * @param symbol char representation of direction.
         * @return direction if symbol is recognizable, null otherwise.
         */
        public static Direction fromSymbol(char symbol) {
            Direction direction = null;
            switch (symbol) {
                case 'E' -> direction = Direction.East;
                case 'S' -> direction = Direction.South;
                case 'O' -> direction = Direction.West;
                case 'N' -> direction = Direction.North;
            }
            return direction;
        }

        /**
         * Get the symbol of this direction.
         *
         * @return symbol of this direction.
         */
        public char toSymbol() {
            char direction;
            switch (this) {
                case East -> direction = 'E';
                case South -> direction = 'S';
                case West -> direction = 'O';
                default -> direction = 'N';
            }
            return direction;
        }
    }

    /**
     * Move is a single move that a Moving Entity object could execute.
     */
    public enum Move {
        Forward, Right, Left;

        /**
         * Get a move from its symbol.
         *
         * @param symbol char representation of move.
         * @return move if symbol is recognizable, null otherwise.
         */
        public static Move fromSymbol(char symbol) {
            Move move = null;
            switch (symbol) {
                case 'A' -> move = Move.Forward;
                case 'D' -> move = Move.Right;
                case 'G' -> move = Move.Left;
            }
            return move;
        }

        /**
         * Get the symbol of this move.
         *
         * @return symbol of this move.
         */
        public Character toSymbol() {
            Character move = null;
            switch (this) {
                case Forward -> move = 'A';
                case Right -> move = 'D';
                case Left -> move = 'G';
            }
            return move;
        }
    }

    /**
     * Moves is a used internally for iterating over the moves of a Moving Entity object.
     */
    private static class Moves {
        private final Move[] moves;
        private int current = 0;

        public Moves(Move[] moves) {
            this.moves = moves == null ? new Move[0] : moves;
        }

        public Move getCurrent() {
            int idx = current;
            if (idx >= moves.length) {
                return null;
            }
            return moves[idx];
        }

        public Move getNext() {
            Move move = getCurrent();
            if (move == null) {
                return null;
            }
            ++current;
            return move;
        }

        @Override
        public String toString() {
            return "Moves{" +
                    "moves=" + Arrays.toString(moves) +
                    ", current=" + current +
                    '}';
        }
    }

}

package edu.aeon.game.component;

/**
 * Savable is a component that holds a representative symbol of an Entity object that could be persisted.
 */
public class Savable implements Component {

    private final char symbol;

    /**
     * Constructor that initializes name.
     *
     * @param symbol representative symbol used for persistence purposes.
     */
    public Savable(char symbol) {
        this.symbol = symbol;
    }

    /**
     * Get the representative symbol used for persistence purposes..
     *
     * @return representative symbol used for persistence purposes.
     */
    public char getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return "Savable{" +
                "symbol=" + symbol +
                '}';
    }

}

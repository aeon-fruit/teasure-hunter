package edu.aeon.game.component;

/**
 * Named is a component that holds a name of an Entity object.
 */
public class Named implements Component {

    private final String name;

    /**
     * Constructor that initializes name.
     *
     * @param name name.
     */
    public Named(String name) {
        this.name = name;
    }

    /**
     * Get name.
     *
     * @return name.
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Named{" +
                "name='" + name + '\'' +
                '}';
    }

}

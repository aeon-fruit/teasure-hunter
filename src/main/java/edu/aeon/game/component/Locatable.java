package edu.aeon.game.component;

/**
 * Locatable is a component that holds a position of an Entity object.
 */
public class Locatable implements Component {

    private int x;
    private int y;

    /**
     * Constructor that initializes position.
     *
     * @param x x position.
     * @param y y position.
     */
    public Locatable(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Get the x position.
     *
     * @return x position.
     */
    public int getX() {
        return x;
    }

    /**
     * Set x position.
     *
     * @param x x position.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Get the y position.
     *
     * @return y position.
     */
    public int getY() {
        return y;
    }

    /**
     * Set y position.
     *
     * @param y y position.
     */
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Locatable{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

}

package edu.aeon.game.component;

/**
 * TreasureHolder is a component that holds the treasure attribute of an Entity object.
 * A treasure attribute has an item count, its change direction (increasing/decreasing) and its ability to change.
 */
public class TreasureHolder implements Component {

    private int count;
    private final boolean incrementing;
    private boolean closed;

    /**
     * Constructor that initializes count and change direction.
     *
     * @param count        item count.
     * @param incrementing true if change direction is increasing, false if it is decreasing.
     */
    public TreasureHolder(int count, boolean incrementing) {
        this.count = Math.max(0, count);
        this.incrementing = incrementing;
        this.closed = (incrementing && count < 0) || (!incrementing && count <= 0);
    }

    /**
     * Get item count.
     *
     * @return item count.
     */
    public int getCount() {
        return count;
    }

    /**
     * Change item count by one according the change direction (increasing/decreasing) if the TreasureHolder is not closed.
     */
    public void changeCount() {
        if (!closed) {
            if (incrementing) {
                ++count;
            } else {
                --count;
                if (count <= 0) {
                    closed = true;
                }

            }
        }
    }

    /**
     * Get the status of the decreasing TreasureHolder: true if still contains items, false otherwise.
     * An increasing TreasureHolder never glimmers.
     *
     * @return false if this is an increasing TreasureHolder or decreasing TreasureHolder that has no items, true otherwise.
     */
    public boolean isGlimmering() {
        return !incrementing && getCount() > 0;
    }

    /**
     * Get the ability to change of a decreasing TreasureHolder: true if it is closed, false other wise.
     * An increasing TreasureHolder is always closed.
     *
     * @return true if this is an increasing TreasureHolder or decreasing TreasureHolder that is closed or that has no items, false otherwise.
     */
    public boolean isClosed() {
        return closed || !isGlimmering();
    }

    /**
     * Close/open a decreasing TreasureHolder if it still has items.
     *
     * @param closed true to close, false to open.
     */
    public void setClosed(boolean closed) {
        if (isGlimmering()) {
            this.closed = closed;
        }
    }

    @Override
    public String toString() {
        return "TreasureHolder{" +
                "count=" + count +
                ", incrementing=" + incrementing +
                ", closed=" + closed +
                '}';
    }

}

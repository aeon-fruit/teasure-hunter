package edu.aeon.game.component;

/**
 * Component holds an attribute of an Entity object. It is one of the three pillars of ECS pattern.
 *
 * @see edu.aeon.game.entity.Entity
 * @see edu.aeon.game.system.System
 */
public interface Component {
}

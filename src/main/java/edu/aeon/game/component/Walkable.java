package edu.aeon.game.component;

/**
 * Walkable is a component that holds a collision state of an Entity object that could stepped on by another Entity object.
 */
public class Walkable implements Component {

    private boolean collision = false;
    private boolean toggled = false;

    /**
     * Default constructor.
     */
    public Walkable() {
    }

    /**
     * Get the collision state.
     *
     * @return true if an Entity object is currently stepping on this Walkable, false otherwise.
     */
    public boolean getCollision() {
        return collision;
    }

    /**
     * Set the collision state and update the flag for first step onto this Walkable.
     * The first step flag is updated according to the transition of the collision state as the following:
     * collision -> no-collision (no longer stepped on): toggled <- false (reset first step).
     * collision -> collision (still stepped on): toggled <- false (no longer on the first step).
     * no-collision -> collision (first time stepped on): toggled <- true (set first step).
     * no-collision -> no-collision (still free): toggled <- false (not stepped on to begin with).
     *
     * @param collision collision state.
     */
    public void collide(boolean collision) {
        this.toggled = collision && !this.collision;
        this.collision = collision;
    }

    /**
     * Get the first step flag.
     * See {@link #collide(boolean) collide} for more details.
     *
     * @return first step flag.
     */
    public boolean isToggled() {
        return toggled;
    }

    @Override
    public String toString() {
        return "Walkable{" +
                "collision=" + collision +
                ", toggled=" + toggled +
                '}';
    }

}

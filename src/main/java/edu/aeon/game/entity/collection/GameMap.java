package edu.aeon.game.entity.collection;

import edu.aeon.game.component.Locatable;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.util.Location;

import java.awt.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * GameMap is a grid that contains tiles and sprites. Tiles and sprites are Entity objects that could only live on one cell in the grid.
 */
public class GameMap {

    private final Cell[] grid;
    private final int width;

    /**
     * Constructor that initializes the grid from a width and a height.
     *
     * @param width  width of the grid.
     * @param height height of the grid.
     */
    public GameMap(int width, int height) {
        width = Math.max(width, 0);
        int count = width * Math.max(height, 0);
        this.width = count == 0 ? 0 : width;
        this.grid = new Cell[count];
        for (int i = 0; i < count; ++i) {
            this.grid[i] = new Cell();
        }
    }

    /**
     * Get dimensions of the grid.
     *
     * @return dimensions of the grid.
     */
    public Dimension getSize() {
        return new Dimension(width, (width == 0) ? 0 : grid.length / width);
    }

    /**
     * Add a tile to the grid. The tile should be a non-null Locatable Entity object.
     * The Locatable component allows the positioning of the tile on the grid.
     *
     * @param tile tile Entity.
     * @return true if the tile was added successfully, false otherwise.
     */
    public boolean addTile(Entity tile) {
        if ((grid.length == 0) || (tile == null)) {
            return false;
        }
        return setTile(tile.getComponent(Locatable.class), tile);
    }

    /**
     * Remove a tile from the grid. The tile should be a non-null Locatable Entity object.
     * The Locatable component allows the locate the tile on the grid.
     *
     * @param tile tile Entity.
     * @return true if the tile was removed successfully, false otherwise.
     */
    public boolean removeTile(Entity tile) {
        if ((grid.length == 0) || (tile == null)) {
            return false;
        }
        return setTile(tile.getComponent(Locatable.class), null);
    }

    /**
     * Get a tile on the grid based on its position.
     *
     * @param x x position of the tile.
     * @param y y position of the tile.
     * @return tile at the given position on the grid, null otherwise.
     */
    public Entity tileAt(int x, int y) {
        if (checkBounds(x, y)) {
            return grid[x + width * y].tile;
        }
        return null;
    }

    /**
     * Add the Id of a sprite to the grid on the given position. The sprite Id should be a non-null Id of an Entity object.
     *
     * @param spriteId sprite Entity Id.
     * @return true if the sprite Id was added successfully, false otherwise.
     */
    public boolean addSprite(int x, int y, Long spriteId) {
        if (checkBounds(x, y) && (spriteId != null)) {
            grid[x + width * y].spriteId = spriteId;
            return true;
        }
        return false;
    }

    /**
     * Get the Id of a sprite on the grid based on its position.
     *
     * @param x x position of the tile.
     * @param y y position of the tile.
     * @return sprite Id at the given position on the grid, null otherwise.
     */
    public Long spriteAt(int x, int y) {
        if (checkBounds(x, y)) {
            return grid[x + width * y].spriteId;
        }
        return null;
    }

    /**
     * Move the sprite Id at a given position on the grid to a new one, if existent.
     *
     * @param oldX old x position.
     * @param oldY old y position.
     * @param newX new x position.
     * @param newY new y position.
     */
    public void moveSprite(int oldX, int oldY, int newX, int newY) {
        if (((newX == oldX) && (newY == oldY)) || !checkBounds(newX, newY) || !checkBounds(oldX, oldY)) {
            return;
        }
        Long oldSpriteId = grid[oldX + width * oldY].spriteId;
        if (oldSpriteId == null) {
            return;
        }
        grid[newX + width * newY].spriteId = oldSpriteId;
        grid[oldX + width * oldY].spriteId = null;
    }

    /**
     * Generate a clipped location on the grid from a given position.
     * If x or y is outside the grid dimensions, clip it to the nearest bound [0, max).
     *
     * @param x x position
     * @param y y position.
     * @return clipped location on the grid based on the given position.
     */
    public Location location(int x, int y) {
        return new Location(Math.max(0, Math.min(x, width - 1)), Math.max(0, Math.min(y, (width == 0) ? 0 : grid.length / width - 1)));
    }

    /**
     * Get a stream of the tiles on the grid.
     * @return stream of the tiles on the grid.
     */
    public Stream<Entity> streamTiles() {
        return Arrays.stream(grid).map(Cell::getTile).filter(Objects::nonNull);
    }

    /**
     * Get a stream of the the Id of the sprites on the grid.
     * @return stream of the Id of the sprites on the grid.
     */
    public Stream<Long> streamSprites() {
        return Arrays.stream(grid).map(Cell::getSpriteId).filter(Objects::nonNull);
    }

    /**
     * Get a stream of the locations on the grid.
     * @return stream of the locations on the grid.
     */
    public Stream<Location> streamLocations() {
        return IntStream.range(0, grid.length).mapToObj(idx -> new Location(idx % width, idx / width));
    }

    private boolean checkBounds(int x, int y) {
        return (x >= 0) && (x < width) && (y >= 0) && (y * width < grid.length);
    }

    private boolean setTile(Locatable location, Entity tile) {
        if (location == null) {
            return false;
        }
        int x = location.getX();
        int y = location.getY();
        if (checkBounds(x, y)) {
            grid[x + width * y].tile = tile;
            return true;
        }
        return false;
    }

    /**
     * Cell is a helper class for holding the entities living on a single grid cell: a tile and sprite Id.
     */
    private static class Cell {
        private Entity tile = null;
        private Long spriteId = null;

        public Cell() {
        }

        public Entity getTile() {
            return tile;
        }

        public Long getSpriteId() {
            return spriteId;
        }

        @Override
        public String toString() {
            return "Cell{" +
                    "tile=" + tile +
                    ", spriteId=" + spriteId +
                    '}';
        }
    }

}

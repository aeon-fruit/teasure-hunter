package edu.aeon.game.entity.collection;

import edu.aeon.game.component.Moving;
import edu.aeon.game.entity.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Sprites is a collections of all the sprites. Sprites are Entity objects that have a Moving component.
 */
public class Sprites {

    private final List<Entity> sprites;

    /**
     * Default constructor that initializes an empty collection.
     */
    public Sprites() {
        this.sprites = new ArrayList<>();
    }

    /**
     * Add a sprite if it is a non-null Moving Entity.
     *
     * @param sprite sprite Entity.
     * @return true if the sprite was added successfully, false otherwise.
     */
    public boolean add(Entity sprite) {
        if ((sprite == null) || !sprite.hasComponent(Moving.class)) {
            return false;
        }
        sprites.add(sprite);
        return true;
    }

    /**
     * Get a sprite Entity by its Id.
     *
     * @param spriteId sprite Entity Id.
     * @return non-null sprite if spriteId is non-null and the Entity exists in the collection, false otherwise.
     */
    public Entity getById(Long spriteId) {
        if (spriteId == null) {
            return null;
        }
        return stream().filter(entity -> entity.getId() == spriteId).findFirst().orElse(null);
    }

    /**
     * Get a stream of all the sprites.
     *
     * @return stream of all the sprites.
     */
    public Stream<Entity> stream() {
        return sprites.stream();
    }

}

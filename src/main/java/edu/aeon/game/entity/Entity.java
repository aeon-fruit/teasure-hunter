package edu.aeon.game.entity;

import edu.aeon.game.component.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Entity is an object with an identifier and a set of Components. It is one of the three pillars of ECS pattern.
 *
 * @see edu.aeon.game.component.Component
 * @see edu.aeon.game.system.System
 */
public class Entity {

    private final long id;
    private final Map<Class<? extends Component>, Component> components;

    /**
     * Constructor that initializes the id and no components.
     *
     * @param id unique id.
     */
    Entity(long id) {
        this.id = id;
        this.components = new HashMap<>();
    }

    /**
     * Get id.
     *
     * @return unique id.
     */
    public long getId() {
        return id;
    }

    /**
     * Decorate this Entity with a Component if non-null.
     *
     * @param component component to attribute to this Entity.
     * @return this Entity with the new component if non-null.
     */
    public Entity withComponent(Component component) {
        if (component != null) {
            components.put(component.getClass(), component);
        }
        return this;
    }

    /**
     * Get a Component by its Class if Class non-null and existent.
     *
     * @param klass class of the Component.
     * @param <T>   Component type.
     * @return Component having the given Class if Class non-null and existent, null otherwise.
     */
    public <T extends Component> T getComponent(Class<T> klass) {
        if (klass == null) {
            return null;
        }
        Component component = components.get(klass);
        if (component == null) {
            return null;
        }
        return klass.isInstance(component) ? klass.cast(component) : null;
    }

    /**
     * Check if this Entity has a Component having the given Class if Class non-null.
     *
     * @param klass class of the Component.
     * @param <T>   Component type.
     * @return true if there is a Component having the given Class if Class non-null, false otherwise.
     */
    public <T extends Component> boolean hasComponent(Class<T> klass) {
        if (klass == null) {
            return true;
        }
        Component component = components.get(klass);
        if (component == null) {
            return false;
        }
        return klass.isInstance(component);
    }

    /**
     * Get a stream of all the Component attributes.
     *
     * @return stream of all the Component attributes.
     */
    public Stream<Map.Entry<Class<? extends Component>, Component>> stream() {
        return components.entrySet().stream();
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                ", components=" + components +
                '}';
    }

}

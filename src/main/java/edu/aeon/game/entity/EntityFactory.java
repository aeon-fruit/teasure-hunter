package edu.aeon.game.entity;

import java.util.concurrent.atomic.AtomicLong;

/**
 * EntityFactory builds vanilla Entity objects with a unique Id.
 */
public class EntityFactory {

    private static final AtomicLong nextId = new AtomicLong();

    /**
     * Get a new Entity object with a unique Id.
     *
     * @return new non-null Entity object with a unique Id.
     */
    public static Entity create() {
        return new Entity(nextId.getAndIncrement());
    }

}

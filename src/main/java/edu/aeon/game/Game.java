package edu.aeon.game;

import edu.aeon.game.component.*;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.EntityFactory;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import edu.aeon.game.system.MovementSystem;
import edu.aeon.game.system.SaveSystem;
import edu.aeon.state.GameStateLoader;
import edu.aeon.state.GameStateSaver;

import java.util.ArrayList;
import java.util.List;

/**
 * Game operates the System objects in a loop of Entity objects update.
 */
public class Game {

    private final MovementSystem movementSystem;
    private final SaveSystem saveSystem;

    /**
     * Constructor that initializes the underlying System objects using  the given map and sprites and game state saver.
     *
     * @param map     map
     * @param sprites sprites
     * @param saver   game state saver
     */
    public Game(GameMap map, Sprites sprites, GameStateSaver saver) {
        this.movementSystem = new MovementSystem(map, sprites);
        this.saveSystem = new SaveSystem(map, sprites, saver);
    }

    /**
     * Update the MovementSystem on a loop until all sprites stand still, then save the final state using the SaveSystem.
     */
    public void loop() {
        do {
            movementSystem.update();
        } while (movementSystem.isAlive());
        saveSystem.update();
    }

    /**
     * Load a new Game using a game state loader and initializes it using the given game state saver.
     *
     * @param loader game state loader
     * @param saver  game state saver
     * @return new Game if the loader and the saver are non-null and the state is loaded properly, null otherwise.
     */
    public static Game load(GameStateLoader loader, GameStateSaver saver) {
        if (loader == null) {
            return null;
        }
        final List<String> lines = loader.load();
        if (lines == null) {
            return null;
        }
        GameState state = new GameState(lines);
        GameMap map = state.map;
        List<Entity> tiles = state.tiles;
        Sprites sprites = state.sprites;
        if (map == null) {
            return null;
        }
        tiles.forEach(map::addTile);
        sprites.stream().forEach(sprite -> {
            Locatable location = sprite.getComponent(Locatable.class);
            if (location == null) {
                return;
            }
            map.addSprite(location.getX(), location.getY(), sprite.getId());
        });
        return new Game(map, sprites, saver);
    }

    /**
     * GameState is a helper class for loading a new game state.
     */
    private static class GameState {

        private GameMap map = null;
        private final List<Entity> tiles = new ArrayList<>();
        private final Sprites sprites = new Sprites();

        public GameState(List<String> lines) {
            // iterate over the loaded lines
            for (String line : lines) {
                line = line.trim();
                if (line.isEmpty() || line.startsWith("#")) {
                    // ignore empty and comment lines
                    continue;
                }
                // parse the line and ignore it if ill-formatted
                String[] tokens = line.split(" - ");
                switch (tokens[0]) {
                    case "C" -> initMap(tokens);
                    case "M" -> parseMountain(tokens);
                    case "T" -> parseTreasure(tokens);
                    case "A" -> parseAdventurer(tokens);
                }
            }
        }

        private void initMap(String[] tokens) {
            if (tokens.length != 3) {
                return;
            }
            try {
                int width = Integer.parseInt(tokens[1]);
                int height = Integer.parseInt(tokens[2]);
                map = new GameMap(width, height);
            } catch (NumberFormatException ignored) {
            }
        }

        private void parseMountain(String[] tokens) {
            if (tokens.length != 3) {
                return;
            }
            try {
                int x = Integer.parseInt(tokens[1]);
                int y = Integer.parseInt(tokens[2]);
                tiles.add(EntityFactory.create()
                        .withComponent(new Savable('M'))
                        .withComponent(new Locatable(x, y)));
            } catch (NumberFormatException ignored) {
            }
        }

        private void parseTreasure(String[] tokens) {
            if (tokens.length != 4) {
                return;
            }
            try {
                int x = Integer.parseInt(tokens[1]);
                int y = Integer.parseInt(tokens[2]);
                int count = Integer.parseInt(tokens[3]);
                tiles.add(EntityFactory.create()
                        .withComponent(new Savable('T'))
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Walkable())
                        .withComponent(new TreasureHolder(count, false)));
            } catch (NumberFormatException ignored) {
            }
        }

        private void parseAdventurer(String[] tokens) {
            // tokens.length = 5 if no moves are provided, 6 otherwise
            if ((tokens.length != 5) && (tokens.length != 6)) {
                return;
            }
            try {
                String name = tokens[1];
                int x = Integer.parseInt(tokens[2]);
                int y = Integer.parseInt(tokens[3]);
                if (tokens[4].length() != 1) {
                    // direction symbol should be one character
                    return;
                }
                Moving.Direction direction = Moving.Direction.fromSymbol(tokens[4].charAt(0));
                if (direction == null) {
                    // failed to parse the direction from the symbol
                    return;
                }
                Moving.Move[] moves = null;
                if (tokens.length == 6) {
                    // parse the moves from a string of move symbols
                    String rawMoves = tokens[5];
                    int count = rawMoves.length();
                    moves = new Moving.Move[count];
                    for (int i = 0; i < count; ++i) {
                        moves[i] = Moving.Move.fromSymbol(rawMoves.charAt(i));
                        if (moves[i] == null) {
                            return;
                        }
                    }
                }
                sprites.add(EntityFactory.create()
                        .withComponent(new Savable('A'))
                        .withComponent(new Locatable(x, y))
                        .withComponent(new Moving(direction, moves))
                        .withComponent(new Named(name))
                        .withComponent(new TreasureHolder(0, true)));
            } catch (NumberFormatException ignored) {
            }
        }

    }


}

package edu.aeon.game.util;

import edu.aeon.game.component.Locatable;

import java.util.Objects;

/**
 * Location is a couple of x and y coordinates on a GameMap.
 *
 * @see edu.aeon.game.entity.collection.GameMap
 */
public class Location extends Locatable {

    /**
     * Constructor that initializes position.
     *
     * @param x x position.
     * @param y y position.
     */
    public Location(int x, int y) {
        super(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Location)) {
            return false;
        }
        Location location = (Location) o;
        return getX() == location.getX() && getY() == location.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "Location{" +
                "x=" + getX() +
                ", y=" + getY() +
                '}';
    }

}

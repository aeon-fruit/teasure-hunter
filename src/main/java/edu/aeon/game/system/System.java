package edu.aeon.game.system;

/**
 * System is a object that updates a subset of the Component attributes of a collection of Entity objects in one iteration.
 * It is one of the three pillars of ECS pattern.
 *
 * @see edu.aeon.game.entity.Entity
 * @see edu.aeon.game.component.Component
 */
public interface System {

    /**
     * Update Entity objects in a single epoch.
     */
    void update();

}

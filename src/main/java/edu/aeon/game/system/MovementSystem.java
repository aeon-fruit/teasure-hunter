package edu.aeon.game.system;

import edu.aeon.game.component.Locatable;
import edu.aeon.game.component.Moving;
import edu.aeon.game.component.TreasureHolder;
import edu.aeon.game.component.Walkable;
import edu.aeon.game.entity.Entity;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import edu.aeon.game.util.Location;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * MovementSystem is a System that iterates over the sprites on a map and updates their locations and their treasure bounty by one move.
 */
public class MovementSystem implements System {

    private final GameMap map;
    private final Sprites sprites;
    private long aliveCount;

    /**
     * Constructor that initializes the map and the sprites to iterate over.
     *
     * @param map map
     * @param sprites sprites
     */
    public MovementSystem(GameMap map, Sprites sprites) {
        this.map = map;
        this.sprites = sprites;
        if ((map != null) && (sprites != null)) {
            // count of the sprites that are still able to move on the map
            this.aliveCount = map.streamSprites().map(sprites::getById)
                    .filter(Objects::nonNull)
                    .filter(sprite -> sprite.hasComponent(Locatable.class))
                    .map(sprite -> sprite.getComponent(Moving.class))
                    .filter(Objects::nonNull)
                    .filter(moving -> moving.peekNextMove() != null)
                    .count();
        }
    }

    /**
     * Check if on the last epoch there were sprites still able to move on the map.
     *
     * @return true if on the last epoch there were sprites still able to move on the map, false otherwise.
     */
    public boolean isAlive() {
        return aliveCount > 0;
    }

    @Override
    public void update() {
        if ((map == null) || (sprites == null)) {
            return;
        }

        // assume that no sprite is still able to move on the map
        aliveCount = 0L;
        // get the sprites living on the map
        List<Entity> spritesOnMap = map.streamSprites().map(sprites::getById).filter(Objects::nonNull)
                .collect(Collectors.toList());
        // move Locatable Moving sprites
        spritesOnMap.stream().filter(sprite -> sprite.hasComponent(Locatable.class))
                .filter(sprite -> sprite.hasComponent(Moving.class))
                .forEachOrdered(this::move);
    }

    private void move(Entity sprite) {
        Moving moves = sprite.getComponent(Moving.class);
        Moving.Move move = moves.getNextMove();
        if (move == null) {
            // this sprite in no longer able to move
            return;
        }
        // we got another living sprite
        ++aliveCount;

        Moving.Direction direction = moves.getDirection();
        switch (move) {
            case Right -> faceRight(moves);
            case Left -> faceLeft(moves);
            case Forward -> moveForward(sprite, direction);
        }
    }

    private void faceLeft(Moving moves) {
        Moving.Direction direction = moves.getDirection();
        if (direction == Moving.Direction.East) {
            moves.setDirection(Moving.Direction.North);
        } else if (direction == Moving.Direction.South) {
            moves.setDirection(Moving.Direction.East);
        } else if (direction == Moving.Direction.West) {
            moves.setDirection(Moving.Direction.South);
        } else if (direction == Moving.Direction.North) {
            moves.setDirection(Moving.Direction.West);
        }
    }

    private void faceRight(Moving moves) {
        Moving.Direction direction = moves.getDirection();
        if (direction == Moving.Direction.East) {
            moves.setDirection(Moving.Direction.South);
        } else if (direction == Moving.Direction.South) {
            moves.setDirection(Moving.Direction.West);
        } else if (direction == Moving.Direction.West) {
            moves.setDirection(Moving.Direction.North);
        } else if (direction == Moving.Direction.North) {
            moves.setDirection(Moving.Direction.East);
        }
    }

    private void moveForward(Entity sprite, Moving.Direction direction) {
        Locatable location = sprite.getComponent(Locatable.class);
        // Compute the new position based on the direction the sprite is facing
        int x = location.getX();
        int y = location.getY();
        int newX = x;
        int newY = y;
        if (direction == Moving.Direction.East) {
            ++newX;
        } else if (direction == Moving.Direction.South) {
            ++newY;
        } else if (direction == Moving.Direction.West) {
            --newX;
        } else if (direction == Moving.Direction.North) {
            --newY;
        }
        // clip the new position to stay on the map
        Location newLocation = map.location(newX, newY);
        newX = newLocation.getX();
        newY = newLocation.getY();

        // detect moving outside the map => stay put
        if ((x == newX) && (y == newY)) {
            return;
        }

        // detect collision with other sprites => stay put
        Long spriteId = map.spriteAt(newX, newY);
        if (spriteId != null) {
            return;
        }

        // detect collision with obstacle (non-Walkable) tiles => stay put
        Entity tile = map.tileAt(newX, newY);
        if ((tile != null) && !tile.hasComponent(Walkable.class)) {
            return;
        }

        // deal with leaving walkable tiles
        exitWalkable(sprite, x, y);

        // deal with entering walkable tiles
        enterWalkable(sprite, newX, newY);

        // move forward on the map
        location.setX(newX);
        location.setY(newY);
        map.moveSprite(x, y, newX, newY);
    }

    private void enterWalkable(Entity sprite, int newX, int newY) {
        Entity tile = map.tileAt(newX, newY);
        if (tile != null) {
            Walkable walkable = tile.getComponent(Walkable.class);
            if (walkable != null) {
                // there is a tile on the new position and it is Walkable, step on it
                walkable.collide(true);
                TreasureHolder treasureChests = tile.getComponent(TreasureHolder.class);
                TreasureHolder spriteInventory = sprite.getComponent(TreasureHolder.class);
                if ((treasureChests != null) && !treasureChests.isClosed() && (spriteInventory != null)) {
                    // the tile is a treasure chest, plunder it
                    treasureChests.changeCount();
                    spriteInventory.changeCount();
                    if (treasureChests.getCount() == 0) {
                        // the treasure chest is exhausted, remove it
                        map.removeTile(tile);
                    }
                }
            }
        }
    }

    private void exitWalkable(Entity sprite, int x, int y) {
        Entity tile = map.tileAt(x, y);
        if (tile != null) {
            Walkable walkable = tile.getComponent(Walkable.class);
            if (walkable != null) {
                // the sprite was on a Walkable tile on the old position, step out of it
                walkable.collide(false);
                TreasureHolder treasureChests = tile.getComponent(TreasureHolder.class);
                if ((treasureChests != null) && treasureChests.isClosed() && sprite.hasComponent(TreasureHolder.class)) {
                    // the tile is a treasure chest, open it for the next treasure pillagers
                    treasureChests.setClosed(false);
                }
            }
        }
    }

}

package edu.aeon.game.system;

import edu.aeon.game.component.*;
import edu.aeon.game.entity.collection.GameMap;
import edu.aeon.game.entity.collection.Sprites;
import edu.aeon.state.GameStateSaver;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SaveSystem is a System that iterates over the sprites on a map and save them using a game state saver.
 */
public class SaveSystem implements System {

    private final GameMap map;
    private final Sprites sprites;
    private final GameStateSaver saver;

    /**
     * Constructor that initializes the map and the sprites to iterate over and the game state saver.
     *
     * @param map     map
     * @param sprites sprites
     * @param saver   game state saver
     */
    public SaveSystem(GameMap map, Sprites sprites, GameStateSaver saver) {
        this.map = map;
        this.sprites = sprites;
        this.saver = saver;
    }

    @Override
    public void update() {
        if ((map == null) || (sprites == null) || (saver == null)) {
            return;
        }

        final List<String> lines = new ArrayList<>();
        Dimension size = map.getSize();
        // header
        lines.add(String.join(" - ", "C", size.width + "", size.height + ""));
        // save only Savable Locatable tiles of the map
        map.streamTiles()
                .filter(tile -> tile.hasComponent(Savable.class))
                .filter(tile -> tile.hasComponent(Locatable.class))
                .forEachOrdered(tile -> {
                    Savable save = tile.getComponent(Savable.class);
                    Locatable location = tile.getComponent(Locatable.class);
                    List<String> sb = new ArrayList<>();
                    sb.add(save.getSymbol() + "");
                    sb.add(location.getX() + "");
                    sb.add(location.getY() + "");
                    TreasureHolder treasure = tile.getComponent(TreasureHolder.class);
                    if (treasure != null) {
                        sb.add(treasure.getCount() + "");
                    }
                    lines.add(String.join(" - ", sb));
                });
        // save only Savable Named Locatable Moving TreasureHolder sprites
        sprites.stream()
                .filter(sprite -> sprite.hasComponent(Savable.class))
                .filter(sprite -> sprite.hasComponent(Named.class))
                .filter(sprite -> sprite.hasComponent(Locatable.class))
                .filter(sprite -> sprite.hasComponent(Moving.class))
                .filter(sprite -> sprite.hasComponent(TreasureHolder.class))
                .forEachOrdered(sprite -> {
                    Savable save = sprite.getComponent(Savable.class);
                    Named name = sprite.getComponent(Named.class);
                    Locatable location = sprite.getComponent(Locatable.class);
                    Moving move = sprite.getComponent(Moving.class);
                    String direction = move.getDirection().toSymbol() + "";
                    TreasureHolder treasure = sprite.getComponent(TreasureHolder.class);
                    lines.add(String.join(" - ", save.getSymbol() + "", name.getName(),
                            location.getX() + "", location.getY() + "", direction, treasure.getCount() + ""));
                });
        saver.save(lines);
    }

}

package edu.aeon.state;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * FileGameState loads and saves the game state from/to files.
 */
public class FileGameState implements GameStateLoader, GameStateSaver {

    private final String input;
    private final String output;

    /**
     * Constructor that initializes the input and the output file name of game state manager.
     *
     * @param input  input file name.
     * @param output output file name.
     */
    public FileGameState(String input, String output) {
        this.input = input;
        this.output = output;
    }

    @Override
    public List<String> load() {
        try {
            return Files.readAllLines(Path.of(input));
        } catch (Exception ignored) {
        }
        return null;
    }

    @Override
    public void save(List<String> lines) {
        if (lines == null) {
            return;
        }
        try {
            Files.write(Path.of(output), lines);
        } catch (Exception ignored) {
        }
    }

}

package edu.aeon.state;

import java.util.List;

/**
 * GameStateLoader loads a game state as a list of lines.
 */
public interface GameStateLoader {

    /**
     * Load a game state.
     *
     * @return list of lines representing the game state.
     */
    List<String> load();

}

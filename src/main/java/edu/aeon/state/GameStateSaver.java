package edu.aeon.state;

import java.util.List;

/**
 * GameStateSaver saves a game state from a list of lines.
 */
public interface GameStateSaver {

    /**
     * Save a game state.
     *
     * @param lines list of lines representing the game state.
     */
    void save(List<String> lines);

}

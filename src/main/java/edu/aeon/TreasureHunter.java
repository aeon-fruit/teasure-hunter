package edu.aeon;

import edu.aeon.game.Game;
import edu.aeon.state.FileGameState;

import java.io.File;
import java.net.URL;

public class TreasureHunter {

    public static void main(String[] args) {
        String input = null;
        String output = null;
        if (args.length > 1) {
            input = args[1];
        }
        if (args.length > 2) {
            output = args[2];
        }
        if (input == null) {
            ClassLoader classLoader = TreasureHunter.class.getClassLoader();
            URL resource = classLoader.getResource("samples/sample_01.in");
            if (resource == null) {
                return;
            }
            input = resource.getFile();
        }
        if (output == null) {
            ClassLoader classLoader = TreasureHunter.class.getClassLoader();
            URL resource = classLoader.getResource("samples");
            if (resource == null) {
                return;
            }
            output = resource.getPath() + File.separator + "sample_01.out";
        }
        FileGameState state = new FileGameState(input, output);
        Game game = Game.load(state, state);
        if (game == null) {
            return;
        }
        game.loop();
    }

}
